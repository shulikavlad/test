<?php

class keyfinder { //по условиям тестового задания скрипт должен быть написан с использованием ООП
	private $url; //адрес сайта, на котором будем искать правильный ответ
	private $from; //с какого числа начинаем поиск
	private $mcurl; //сюда будет помещен curl_multi
	private $chtemplate; //основной шаблон curl экзепляра, на основе которого будут созданы остальные
	private $channels; //здесь будут храниться экзепляры наших curl
	private $result; //сюда будет помещен результат, в случае его нахождения
	private $start_time; //сюда будет помещено время старта для анализа общего времени выполнения скрипта
	
	public function __construct ($from) {
		set_time_limit(3000); //скрипт должен найти ответ не более чем за 30 минут
		
		$this->start_time = microtime(true);
		$this->url = "http://www.rollshop.co.il/test.php";
		$this->from = (int)$from;
		$this->mcurl = curl_multi_init();
		$this->channels = [];
		
		$this->chtemplate = curl_init(); //чтобы не инициализировать каждый раз новый сеанс cURL, инициализируем однажды и будем создавать новые сеансы на основе этого
		curl_setopt($this->chtemplate, CURLOPT_URL, $this->url); 		//
		curl_setopt($this->chtemplate, CURLOPT_POST, 1);				//устанавливаем значения опций, которые будут одинаковы для всех объектов
		curl_setopt($this->chtemplate, CURLOPT_RETURNTRANSFER, true);	//	
		curl_setopt($this->chtemplate, CURLOPT_CONNECTTIMEOUT, 6000);	//	
		curl_setopt($this->chtemplate, CURLOPT_TIMEOUT, 6000);			//	
	}
	
	private function searchFrom ($value) { //функция готовит запросы в заданном диапазоне и объединяет их в curl_multi
		$this->msg('Preparation of queries has been started!');
		
		$this->channels = [];
		for ($i = $value; $i <= ($value + 100); $i++) {
			$temp_ch = curl_copy_handle($this->chtemplate);
			curl_setopt($temp_ch, CURLOPT_POSTFIELDS, 'code='.$i);
			curl_multi_add_handle($this->mcurl, $temp_ch);
			$this->channels[$i] = $temp_ch;
		}
		
		return $this->startSearch();
	}
	
	private function startSearch() { //выполнение curl_multi запуск анализа полученных данных
		$this->msg('Performing queries has been started!');
		
		$active = null;
		do {
			$mrc = curl_multi_exec($this->mcurl, $active); //так как ответ на 1 запрос к серверу составляет около 5 сек. было решено послать запросы асинхронно
		} while ($mrc == CURLM_CALL_MULTI_PERFORM);
		 
		while ($active && $mrc == CURLM_OK) {
			if (curl_multi_select($this->mcurl) == -1) {
				continue;
			}

			do {
				$mrc = curl_multi_exec($this->mcurl, $active);
			} while ($mrc == CURLM_CALL_MULTI_PERFORM);
		}
		
		return $this->analyseResults(); //выполнение запросов завершено, можно приступать к анализу полученных данных
	}
	
	private function analyseResults () { 
		$this->msg('Analysing of queries results has been started!');
		foreach ($this->channels as $key => $channel) { //перебираем все выполненные запросы
			$response_code = curl_getinfo($channel, CURLINFO_RESPONSE_CODE);
			
			if ($response_code != 200) { //если ответ сервера не 200 - пропускаем
				continue;
			}
			
			$response = curl_multi_getcontent($channel);
			if (stristr($response, 'WRONG') !== false) //если в полученном ответе присутствует WRONG - ответ неверный, пропускаем
				continue;
				
			curl_multi_remove_handle($this->mcurl, $channel); //удаляем канал с curl_multi
			
			$this->result = str_replace([" ", "\n"], ['',''], strip_tags($response)); //убираем теги, пробелы и переносы строк
			$this->msg('Right answer: ' . $key); //выводим правильный ответ в консоль
			$this->msg('Content of page with right answer(stripped html): ' . $this->result); //выводим содержимое страницы с правильным ответом в консоль
			break;
		}
		if (isset($response))
			return $response;
	}
	
	public function search () {
		$this->msg('Search has been started from ' . $this->from);
		$iterationcount = 0;
		
		do {
			$this->msg('Iteration # ' . $iterationcount . ' has been started!');
			$this->searchFrom($this->from + ($iterationcount * 100)); //ищем ответы начиная с заданого числа, посылаем по 100 запросов
			$iterationcount++;
			sleep(5);
		} while (!$this->result); //выполняем пока не получим результат
		
		return $this->result;
	}
	
	private function msg($text) {
		echo PHP_EOL . $text;
	}
	
	public function __destruct () {
		$sec = microtime(true) - $this->start_time; //считаем общее время выполнения скрипта
		$this->msg('Script execution took: ' . $sec . ' s.' . PHP_EOL); //выводим время выполнения в консоль
		
		curl_multi_close($this->mcurl);
	}
}

$from = isset($_GET['from']) ? $_GET['from'] : (isset($argv[1]) ? $argv[1] : 0); //получаем начальное значения, с которого начнем поиск
$keyfinder_obj = new keyfinder($from); //создаем новый экземпляр нашего класса
$keyfinder_obj->search(); //запускаем поиск